const Sequelize = require('sequelize');
require('sequelize-values')(Sequelize);
const path = require('path');
const fs = require('fs');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'dev';
const config = require(__dirname + '/libs/sequelize')[env];
let db = {};
let models = {};
module.exports = app => {
  let sequelize;
  if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
  } else {
    sequelize = new Sequelize(config.database, config.username, config.password, config);
  }
  const dir = path.join(__dirname, 'db/models');
  fs
    .readdirSync(dir)
    .filter(file => {
      return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
      const model = sequelize['import'](path.join(dir, file));
      models[model.name] = model;
    });
  Object.keys(models).forEach(modelName => {
    if (models[modelName].associate) {
      models[modelName].associate(models);
    }
  });
  models.topic.hasMany(models.subtopic, {foreignKey: 'id_topic', sourceKey: 'id'});
  models.subtopic.belongsTo(models.topic, {foreignKey: 'id_topic', targetKey: 'id'});

  models.subtopic.hasMany(models.resource, {foreignKey: 'id_subtopic', sourceKey: 'id'});
  models.resource.belongsTo(models.subtopic, {foreignKey: 'id_subtopic', targetKey: 'id'});

  db.models = models;
  db.sequelize = sequelize;
  db.Sequelize = Sequelize;
  return db;
};
