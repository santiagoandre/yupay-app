module.exports = {
  dev: {
    database: "db.sqlite",
    dialect: "sqlite",
    storage: "src/db/db.sqlite"
  },
  test: {
    database: "db.sqlite",
    dialect: "sqlite",
    storage: "src/db/db.sqlite"
  },
  production: {
    database: "db.sqlite",
    dialect: "sqlite",
    storage: "src/db/db.sqlite"
  }
  // production: {
  //   url: process.env.DATABASE_URL,
  //   dialect: 'postgres',
  //   use_env_variable: "DATABASE_URL",
  //   dialectOptions: {
  //     ssl: {
  //         rejectUnauthorized: false
  //     }
  //   }

  // }
}
