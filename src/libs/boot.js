
module.exports = app => {
  //app.get('/answer', app.routes.answerController);
  app.db.sequelize.sync().done(() => {
    app.listen(app.get('port'), () => {
      console.log('Server on port', app.get('port'));
    });
  });

};
