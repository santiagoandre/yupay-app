const TEMP_FOLDER = '.temp';
const multer  = require('multer');
const rimraf = require("rimraf");
const fs = require('fs');
//init temp folder, if exist, remove this content
if (fs.existsSync(TEMP_FOLDER)) {
  rimraf.sync(TEMP_FOLDER);
}
fs.mkdirSync(TEMP_FOLDER);

const storage = multer.diskStorage({ // notice you are calling the multer.diskStorage() method here, not multer()
    destination: function(req, file, cb) {
        cb(null,TEMP_FOLDER);
    },
    filename: function(req, file, cb) {
        cb(null, file.originalname + Math.round(Math.random() * 100));
    }
});
module.exports = app =>{
  app.uploadMulter = multer({storage});
}
