const fs = require('fs');
const path = require('path');
module.exports = app => {
  var paths = {}
  paths.ASSETS_PATH= 'assets';
  paths.QUESTION_IMAGES_PATH= path.join(paths.ASSETS_PATH, 'questions');
  paths.ANSWER_IMAGES_PATH= path.join(paths.ASSETS_PATH, 'answers');

  paths.PDF_PATH= path.join(paths.ASSETS_PATH, 'pdf');
  paths.VIDEO_PATH= path.join(paths.ASSETS_PATH, 'video');
  paths.THUMBNAILS_PATH= path.join(paths.ASSETS_PATH, 'thumbnails');
  for (var i in paths) {
    const dir = paths[i];
    if (!fs.existsSync(dir)) {
      console.log("Create folder: "+dir);
      fs.mkdirSync(dir, 0744);
    }
  }
  app.paths = paths;
}
