 const express =require('express');
const session =require('express-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const cors =  require('cors');


const AdminBro = require('admin-bro');
const AdminBroExpress = require('admin-bro-expressjs');
//const secure = require('express-force-https');
module.exports = app => {
  //https
  //app.use(secure);
  // Settings
  app.set('port', process.env.PORT || 8080,'0.0.0.0');
  app.set('json spaces', 4);

  //req.body req.dsdfs
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended:true}));
  // middlewares
  const SECRET_KEY = process.env.SECRET_KEY || 'secret';
  console.log("SECRET_KEY:");
  console.log(SECRET_KEY);
  app.use(session({
    secret:  SECRET_KEY,
    resave: true,
    saveUninitialized: true
  }));
  //admin view
  const adminBro = new AdminBro(app.admin.index);
  const router = AdminBroExpress.buildRouter(adminBro);
  app.use(adminBro.options.rootPath, router);

  //no se caigan las peticiones de los clientes
  app.use(cors());
  //sesiones
  app.use(passport.initialize());
  app.use(passport.session());

  app.use(express.json());

  app.use((req, res, next) => {
    //res.header("Access-Control-Allow-Origin", "*");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    // delete req.body.id;
    next();
  });


};
