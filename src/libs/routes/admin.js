const path = require('path');
module.exports = app => {
  app.get('/', function(req, res) {
    res.redirect('/admin');
  });
  app.get('/resource', app.controllers.resourceController.all);
  app.get('/subtopic', app.controllers.subtopicController.all);
  app.post('/question',app.uploadMulter.array('images',5), app.controllers.questionController.insert);
  app.post('/resource',app.uploadMulter.array('files',2), app.controllers.resourceController.insert);

}
