const fs = require('fs');
module.exports = app => {
  //  console.log("load controllers");
  const isAuthenticated = app.libs.passport.isAuthenticated;
  //console.log(isAuthenticated);

  app.post('/api/login', app.controllers.sessionController.login);
  app.post('/logout', isAuthenticated, app.controllers.sessionController.logout);
  app.post('/api/register', app.controllers.sessionController.signup);



  app.get('/api/content', app.controllers.contentController.all);



  app.get('/api/view/:id', app.controllers.resourceController.addReview);
  app.get('/api/pdf/:id', app.controllers.resourceController.getPdf);
  app.get('/api/video/:id', app.controllers.resourceController.getVideo);
  app.get('/api/thumbnails/:id', app.controllers.resourceController.getThumbnails);

  app.get('/api/question/resource/:resource_id', app.controllers.questionController.getResourceQuestions);
  app.get('/api/question/img/:id', app.controllers.questionController.getQuestionImage);
  app.get('/api/answer/:id_question/:id_answer', app.controllers.questionController.getAnswerImage);
  app.get('/api/logo', (req, res)=> {
    console.log('get logo');
    const filePath = 'assets/icons/logo.png';
    const stat = fs.statSync(filePath);
    const fileSize = stat.size;
    const head = {
      'Content-Length': fileSize,
      'Content-Type': 'image/png',
    };
    res.writeHead(200, head);
    fs.createReadStream(filePath).pipe(res);
  });

  app.post('/api/log', (req, res) => {
    console.log('post api/log');
    res.json({
      msg: 'success'
    });
  })




}
