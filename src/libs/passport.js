const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

module.exports = app =>{
  //console.log("serializeUser");
  const User =app.db.models.user;
  passport.serializeUser((user,done) => {
    done(null,user.id);
  })

  passport.deserializeUser((id,done) => {
    //console.log("deserializeUser");
    User.findOne({where:{ id: id}})
    .then( user =>user)
    .catch(err => err);
  });

  passport.use(new LocalStrategy(
    {usernameField: 'username'},
    (username,password,done)=> {
      User.findOne({where: {username:username}}).then(user  => {
        console.log("find user");
        if(!user){
          return done(null,false,{message:`username: ${username} unregistered`});
        }
        //console.log(user);
        user.verifyPassword(password,(err,equals) => {
          if(equals){
            return done(null,user);
          }else{
            return done(null,false, {message:` incorrect password`});
          }
        });


      });

    }
  ));

  return {
    isAuthenticated : (req,res,next) => {
        if(req.isAuthenticated()){
          //console.log("Authenticated");
          return next(null,true);
        };
        res.status(401).send('Unauthorized');
    }
  }

}
