const fs = require('fs');
const path = require('path');

module.exports = app => {
  const fileController = app.controllers.fileController;
  const ResourceDAO = app.DAO.resourceDAO;
  var PDF_PATH =  app.paths.PDF_PATH;
  var VIDEO_PATH = app.paths.VIDEO_PATH;
  var THUMBNAILS_PATH = app.paths.THUMBNAILS_PATH;
  const controller = {};
  controller.all = (req, res) => {
    ResourceDAO.all()
      .then(result => res.json(result))
      .catch(error => {
        res.status(412).json({
          msg: error.message
        });
      });
  };
  controller.insert = (req, res) => {
    console.log("insert resource")
    const [thumbnail,file]= req.files
    let typefile = file.mimetype.split("/");
    if(typefile[0] == 'video'){
      typefile = 'video'
    }else{
      typefile = 'pdf';
    }
    let params = req.body
    params['type'] = typefile
    ResourceDAO.insert(params)
      .then(newResource => {
        //step two: save files
        if (!saveFiles(newResource.id, thumbnail, file)) {
          console.log("error saving files");
          newResource.delete();
          res.status(500).send();
          return;
        }
        console.log("Success");
        res.status(200).send();

      });
    };


  controller.addReview = (req, res) => {
    const id = req.params.id;
    console.log("get addReview " + id);
    ResourceDAO.find(id).then(resource => {
      if (!resource) {
        console.log("resource not found");

        res.status(404).json({
          msg: 'resource not found'
        });
        return;
      }
      resource.update({
        views: resource.views + 1
      }).then(() => res.status(200).json({
        msg: 'success'
      }));
    }).catch(err => {
      console.log(err);
      res.status(204).json({
        msg: 'resource not found'
      });
    });
  };
  controller.getPdf = (req, res) => {
    const id = req.params.id;
    console.log('get /api/pdf/' + id);
    ResourceDAO.find(id).then(resource => {
      if (!resource) {
        console.log("resource not found");

        res.status(404).json({
          msg: 'resource not found'
        });
        return;
      }
      const filePath = path.join(PDF_PATH, id.toString() + '.pdf');
      const head = fileController.createHeaders(filePath);
      res.writeHead(200, head);
      fs.createReadStream(filePath).pipe(res);
    }).catch(err => console.log(err));
  };
  controller.getVideo = (req, res) => {
    const id = req.params.id;
    console.log('get /api/video/' + id);
    ResourceDAO.find(id).then(resource => {
      if (!resource) {
        console.log("resource not found");

        res.status(404).json({
          msg: 'resource not found'
        });
        return;
      }
      const files = fileController.filterWithoutExt(VIDEO_PATH,id.toString());
      if(files.length ==0){
        //error image not found
        res.status(412).json({msg:"Interal server error"}).send();
        return;
      }
      const filePath = path.join(VIDEO_PATH, files[0]);//send only first resource
      if(!fileController.isVideo(filePath)){
        console.log("error resource is not a video");
        res.status(412).json({msg:"Interal server error"}).send();
        return;
      }
      const stat = fs.statSync(filePath);
      const fileSize = stat.size;
      const range = req.headers.range;
      if (range) {
        const parts = range.replace(/bytes=/, '').split('-');
        const start = parseInt(parts[0], 10);
        const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
        const chunksize = end - start + 1;
        const file = fs.createReadStream(filePath, {
          start,
          end
        });
        const head = {
          'Content-Range': `bytes ${start}-${end}/${fileSize}`,
          'Accept-Ranges': 'bytes',
          'Content-Length': chunksize,
          'Content-Type': fileController.mimetypeFile(filePath)
        };
        res.writeHead(206, head);
        file.pipe(res);
      } else {
        const head = fileController.createHeaders(filePath);
        res.writeHead(200, head);
        fs.createReadStream(filePath).pipe(res);
      }
    }).catch(err => console.log(err));
  };
  controller.getThumbnails = (req, res) => {
    const id = req.params.id;
    console.log('get /api/thumbnails/' + id);

    const files = fileController.filterWithoutExt(THUMBNAILS_PATH,id.toString());
    if(files.length ==0){
      //error image not found
      res.status(412).json({msg:"Interal server error"}).send();
      return;
    }
    const filePath = path.join(THUMBNAILS_PATH, files[0]);
    const head = fileController.createHeaders(filePath);
    res.writeHead(200, head);
    fs.createReadStream(filePath).pipe(res);
  };
  function saveFiles(resourceId,thumbnail,file){
    const typefile = file.mimetype.split("/");
    var dir;
    if(typefile[0] == 'video'){
      dir =   VIDEO_PATH;
    }else{
      dir = PDF_PATH;
    }
    const questionPath = path.join(dir,resourceId + "." + typefile[1]);
    fs.rename(file.path, questionPath, function(err) {
      if (err) {
        console.log(err);
        return false;
      }
    });
    const thumbnailPath = path.join(THUMBNAILS_PATH,resourceId + "." + thumbnail.mimetype.split("/")[1]);
    fs.rename(thumbnail.path, thumbnailPath, function(err) {
      if (err) {
        console.log(err);
        return false;
      }
    });
    return true;
  }
  return controller;
}
