module.exports = app => {

  const User = app.DAO.userDAO;
  const controller= {};
  controller.all =(req, res) => {
      User.all()
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({msg: error.message});
        });
      };
    controller.insert =(req, res) => {

      User.insert(req.query)
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({msg: error.message});
        });
    };
    return controller;


};
