module.exports = app => {

  const Course = app.DAO.courseDAO;
  const controller = {};
  controller.all =(req, res) => {
      Course.all()
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({msg: error.message});
        });
    };

    controller.insert =(req, res) => {
      Course.insert(req.query)
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({msg: error.message});
        });
    };
    return controller;


};
