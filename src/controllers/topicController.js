module.exports = app => {

  const Topic = app.DAO.topicDAO;
  const controller= {};
  controller.all =(req, res) => {
      Topic.all()
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({msg: error.message});
        });
    };
    controller.insert =(req, res) => {

      Topic.insert(req.query)
        .then(result => res.json(result))
        .catch(error => {
          console.log(res);
          res.status(412).json({msg: error.message});
        });
    };
    return controller;

};
