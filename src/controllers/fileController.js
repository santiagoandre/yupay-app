const mime = require('mime');
const fs = require('fs');
const controller = {};
controller.filterWithoutExt = function(dir, nameFile) {
  return fs
    .readdirSync(dir)
    .filter(file => {
      return file.split('.')[0] == nameFile
    })
};
controller.fileSize = function(filePath) {
  const stat = fs.statSync(filePath);
  return stat.size;
};
controller.mimetypeFile= function(filePath) {
  return mime.getType(filePath);
};
controller.createHeaders = function(filePath) {
  const fileSize = controller.fileSize(filePath);
  const mimetype = controller.mimetypeFile(filePath);
  return {
    'Content-Length': fileSize,
    'Content-Type': mimetype
  };

};
controller.isVideo = function(filePath){
  return controller.mimetypeFile(filePath).split("/")[0] == "video";
}
module.exports = controller;
