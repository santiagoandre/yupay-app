module.exports = app => {

  const Subtopic = app.DAO.subtopicDAO;
  const controller= {};
  controller.all =(req, res) => {
      Subtopic.all()
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({msg: error.message});
        });
    };

    controller.insert =(req, res) => {

      Subtopic.insert(req.query)
        .then(result => res.json(result))
        .catch(error => {
          //console.log(res);
          res.status(412).json({msg: error.message});
        });
    };

    return controller;
};
