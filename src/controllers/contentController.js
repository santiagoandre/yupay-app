module.exports = app => {
  const controller = {};
  controller.all = (req, res)=>{
    app.DAO.contentDAO.all().then(all =>{
      all  = app.db.Sequelize.getValues(all);
      console.log(all);
      res.status(200).json({topics: all});
    }).catch(err =>{
      console.log(err);

    });
  }

  return controller;
}
