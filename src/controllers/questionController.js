const fs = require('fs');
const path = require('path');


module.exports = app => {

  const fileController = app.controllers.fileController;
  const Question = app.DAO.questionDAO;
  const QUESTION_IMAGES_PATH =  app.paths.QUESTION_IMAGES_PATH;
  const ANSWER_IMAGES_PATH = app.paths.ANSWER_IMAGES_PATH;
  const controller = {};
  controller.all = (req, res) => {
    Question.all()
      .then(result => res.json(result))
      .catch(error => {
        res.status(412).json({
          error: error.message
        });
      });
  };
  controller.insert = (req, res) => {
    console.log("post question");
    //console.log("fist: get params");
    let question = req.files[0];
    let answers = req.files.slice(1, 5);
    const resourceId = parseInt(req.body.resource);
    const responseTime = parseInt(req.body.time);
    const rightIndex = parseInt(req.body.rightIndex);
    console.log(req.body);
    console.log(rightIndex);
    //console.log("two: insert in database");
    Question.insert({id_resource:resourceId,responseTime:responseTime,rightIndex:rightIndex}).then(newQuestion =>{
      //console.log("three: save images");
      if (!saveImages(newQuestion.id, question, answers)) {
        console.log("error saving images");
        newQuestion.delete();
        res.status(500).send();

        return;
      }
      console.log("Success");
      res.status(200).send();
    });


  };
  controller.getResourceQuestions = (req, res) => {
    const resource_id = req.params.resource_id;
    console.log(`get /api/question/resource/${resource_id}`);
    Question.select(resource_id).then(questions => {
      res.status(200).json(questions);
    }).catch((err) => res.json({
      err: err
    }));
  };
  controller.getQuestionImage = (req, res) => {
    const id_question = req.params.id;
    console.log(`get /api/question/img/${id_question}`);
    if(!id_question){
      res.status(412).send();
      return;
    }
    const files = fileController.filterWithoutExt(QUESTION_IMAGES_PATH,id_question.toString());
    if(files.length ==0){
      //error image not found
      res.status(412).json({msg:"Interal server error"}).send();
      return;
    }
    console.log("found image, send first");
    const filePath = path.join(QUESTION_IMAGES_PATH, files[0]);
    const head = fileController.createHeaders(filePath);
    res.writeHead(200, head);
    fs.createReadStream(filePath).pipe(res);
  };
  controller.getAnswerImage = (req, res) => {
    const id_question = req.params.id_question;
    const id_answer = req.params.id_answer;
    console.log(`/api/answer/${id_question}/${id_answer}`);
    const files = fileController.filterWithoutExt(ANSWER_IMAGES_PATH, id_question.toString() + '-' + id_answer.toString());
    if(files.length ==0){
      //error image not found
      res.status(412).json({msg:"Interal server error"}).send();
      return;
    }
    console.log("found image, send first");
    const filePath = path.join(ANSWER_IMAGES_PATH, files[0]);
    const head = fileController.createHeaders(filePath);
    res.writeHead(200, head);
    fs.createReadStream(filePath).pipe(res);
  };


  function saveImages(questionId,questionImage,answerImages){
    //console.log("mv "+ questionImage.path+" > "+  'assets/questions/' + resourceId + "." + questionImage.mimetype.split("/")[1]);
    //console.log(questionId);
    var  newPath  = path.join( QUESTION_IMAGES_PATH , questionId + "." + questionImage.mimetype.split("/")[1]);
    fs.rename(questionImage.path,newPath, function(err) {
      if (err) {
        console.log(err);
        return false;
      }

    });
    for (var answer_number in answerImages) {
      var answer = answerImages[answer_number];
      var oldPath = answer.path;
      newPath = path.join(ANSWER_IMAGES_PATH, questionId + "-" + answer_number + "." + answer.mimetype.split("/")[1]);
      fs.rename(oldPath, newPath, function(err) {
        if (err) {
          console.log(err);
          return false;

        }
      });
    }
    return true;
  }


  return controller;

};
