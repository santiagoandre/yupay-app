module.exports = app => {

  const answer = app.DAO.answerDAO;
  const answerController = {};
  answerController.all = async (req,res) => {

    answer.all()
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({msg: error.message});
        });
   }
  answerController.insert = (req,res) => {
    answer.insert(req.query)
        .then(result => res.json(result))
        .catch(error => {
          console.log(res);
          res.status(412).json({msg: error.message});
        });
  }
  return answerController;

};
