const AdminBro = require('admin-bro')
const sequelizeAdapter = require('admin-bro-sequelizejs')
AdminBro.registerAdapter(sequelizeAdapter)
const userResource = require('./resources/user')
const topicResource = require('./resources/topic')
const subtopicResource = require('./resources/subtopic')
const courseResource = require('./resources/course')
const resourceResource = require('./resources/resource')
const questionResource = require('./resources/question')
const actions = require('./defaultActions')

const menu = {
  courses: { name: 'Cursos', icon: 'fas fa-chalkboard-teacher' },
  users: { name: 'Usuarios', icon: 'fas fa-users' },
  contents: { name: 'Contenidos', icon: 'fas fa-cubes' },
  questions: { name: 'Preguntas', icon: 'far fa-question-circle' }
}
const parent = {
  name: '  ',
  icon: ' ',
}



module.exports =  app => { return {
  databases: [app.db],
  resources: [
    { resource: app.db.models.user, options: {...userResource , parent: parent}},
    { resource: app.db.models.course, options: {...courseResource, actions:actions, parent: parent}},
    { resource: app.db.models.question, options: {...questionResource , parent: parent}},
    { resource: app.db.models.topic, options: { ...topicResource, actions:actions,parent: menu.contents} },
    { resource: app.db.models.subtopic, options: { ...subtopicResource, actions:actions,parent: menu.contents } },
    { resource: app.db.models.resource, options: { ...resourceResource ,parent: menu.contents } },
  ],
  version: {
    admin: false,
  },
  dashboard: {
    component: AdminBro.bundle('./components/dashboard')
  },
  branding: {
    companyName: 'Yupay',
    logo: 'api/logo',
    softwareBrothers:false
  },
}
}
