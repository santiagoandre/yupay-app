

function prepareReq(request){
  let formData = new FormData()
  for (var key in request) {
    var value = request[key]
    if (key == 'thumbnail' || key == 'file') {
      key = 'files'
    }
    formData.append(key, value)

  }
  return formData
}

function prepareFiles(formData,label,files){
  for (var i in files) {
        formData.append(label,files[i])
  }
  return formData
}
module.exports = {
  edit: (resourceEdit) => {

    console.log("==========Antes============")
    return axios.put('/resource', prepareReq(resourceEdit), {})
    console.log("===============Despues============")
  },
  new: (resource) => (

    axios.post('/resource', prepareReq(resource), { // receive two parameter endpoint url ,form data
    })

  ),
  getResources: ()=>(axios.get('/resource'))

}
