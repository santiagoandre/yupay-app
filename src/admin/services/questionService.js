function prepareReq(request){
  console.log(request)
  let formData = new FormData()
  for (var key in request) {
      var value = request[key]
      if(key == 'images'){
        formData = prepareImages(formData,key,value)
      }else{
      //  console.log("Append"+key)
        formData.append(key,value)
      }
    //  console.log(formData)
  }
  return formData
}

function prepareImages(formData,label,images){
  for (var i in images) {
    //  console.log(images[i])
    formData.append(label,images[i])
  }
  return formData
}
module.exports = {
  edit: (questionEdit) => {

    console.log("==========Antes============")
    return axios.put('/question', prepareReq(questionEdit), {})
    console.log("===============Despues============")
  },
  new: (question) => {

    return axios.post('/question', prepareReq(question), { // receive two parameter endpoint url ,form data
    })

  }
}
