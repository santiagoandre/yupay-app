import React from 'react'
import styled from 'styled-components'
export default class Dashboard extends React.Component {
  constructor(props) {
    super(props)
  }



  render() {
    const LogoImg = styled.img.attrs({
        src: 'api/logo',
        alt: 'Logo',
        className: 'center'
      })`
        width: 350px;
        display: block;
        margin-left: auto;
        margin-right: auto;
    `
    const Title = styled.h2`
        font-size: 54px;
        font-weight: bold;
        text-align: center;
      `

      const Lorem = styled.p`
          font-size: 20px;
          text-align: center;
        `


    return (
      <React.Fragment>
        <LogoImg />
        <Title> Yupay App </Title>
        <Lorem>Cras facilisis dapibus magna placerat consequat molestie ornare integer mattis dis tristique convallis, scelerisque primis luctus turpis fermentum pellentesque neque commodo interdum dui.  </Lorem>
      </React.Fragment>
    )
  }

}
