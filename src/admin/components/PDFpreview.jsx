import pdfjsLib from 'pdfjs-dist/webpack'
import styled from 'styled-components'

export default class PDFpreview extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      numPages: null,
      pageNumber: 1,
    }
    console.log("inn pdf component");
    console.log(props.pdf)
    this.onDocumentLoadSuccess = this.onDocumentLoadSuccess.bind(this)
    this.nextPage = this.nextPage.bind(this)
    this.prevPage = this.prevPage.bind(this)
    this.loadPdf = this.loadPdf.bind(this)
    this.loadPdf()

  }
  loadPdf(){
    console.log("load")
  		pdfjsLib.getDocument(this.props.pdf).then(function(pdf) {
    			// you can now use *pdf* here
    		console.log("the pdf has ",pdf.numPages, "page(s).")


    })
}
  onDocumentLoadSuccess(event) {
    let numPages = event.numPages
    this.setState({ numPages: numPages })
  }

  nextPage() {
    this.setState({
      pageNumber:
        this.state.pageNumber === this.state.numPages
          ? this.state.pageNumber
          : this.state.pageNumber + 1,
    })
  }

  prevPage() {
    this.setState({
      pageNumber: this.state.pageNumber === 1 ? 1 : this.state.pageNumber - 1,
    })
  }

  render() {
    const { pageNumber, numPages } = this.state

    return (
      <PDFContainer>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js">
        </script>
        {this.props.pdf && (
          <>
            <DocumentContainer>
            </DocumentContainer>
            <PDFControls>
              <BackBtn onClick={this.prevPage}>Página Anterior</BackBtn>
              <h4
                style={{
                  marginTop: '0px',
                  backgroundColor: 'transparent',
                  paddingLeft: '5px',
                }}
              >
                {' '}
                Página {pageNumber} de {numPages}
              </h4>
              <NextBtn onClick={this.nextPage}> Página Siguiente</NextBtn>
            </PDFControls>
          </>
        )}
      </PDFContainer>
    )
  }
}

const PDFContainer = styled.div`
  width: 700px;
  height: 394px;
  position: relative;
`
const BackBtn = styled.button`
  background-color: #FFF0;
  color: #000;
  font-weight: 'bold';
`
const NextBtn = styled.button`
  font-weight: '700';
`
const DocumentContainer = styled.div`
  width: 700px;
  height: 394px;
  overflow-y: scroll;
`

const PDFControls = styled.div`
  position: absolute;
  bottom: 10px;
  right: 100px;
  width: 250px;
  height: 40px;
  display: flex;
`
