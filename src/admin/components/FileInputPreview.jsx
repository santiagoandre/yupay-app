import React from 'react'
import styled from 'styled-components'
//import PDFpreview from './PDFpreview'
export default class FileInputPreview extends React.Component {
  constructor(props) {
    super(props)
    //accept,

    this.state = {
        file: null
      }
    this.fileSelectedHandler = this.fileSelectedHandler.bind(this)
  }

  fileSelectedHandler(event){
    let file = event.target.files[0]
    //console.log(file)
    const type = file.type.split('/')
    //console.log(file)
    this.setState({type: type[0],subtype: type[1], file:URL.createObjectURL(file)})
    this.props.onChange(file);
  }

  render() {


    return (
      <Wrapper max-width={this.props.maxWidth? this.props.maxWidth : "700px"} >
            {!this.state.file ?
              (<BtnUpload> {this.props.label} </BtnUpload>)
            :this.state.type == 'image' && this.state.subtype != 'svg'?
              ( <Img  src={this.state.file}  alt='Imagen seleccionada' />)
            :this.state.type == 'video'?
              ( <Video   src={this.state.file} controls={true}/> )
            :
              (  <BtnUpload> Archivo {this.state.subtype} seleccionado </BtnUpload>)
            }
            <InputFile type='file'  accept={this.props.accept} onChange={this.fileSelectedHandler}/>

      </Wrapper>
    )


  }

}

const Wrapper = styled.div
`
  overflow: hidden;
  position:relative;
  padding: 10px 10px;
  margin-left: auto;
  margin-right: auto;
  width:100%;
`
const Img = styled.img
`
  margin-left: auto;
  margin-right: auto;
  display:block;
`
const Video = styled.video
`
  margin-left: auto;
  margin-right: auto;
  display:block;
`

const InputFile = styled.input
`

  font-size: 100px;
  opacity: 0;
  width:100%;
  height:80%;
  margin-left: auto;
  margin-right: auto;
  position:absolute;
  top:0; left:0;
`

const BtnUpload = styled.div
`
  border: 2px solid gray;
  color: gray;
  background-color: white;
  padding: 8px 20px;
  border-radius: 8px;
  font-size: 20px;
  font-weight: bold;
  text-align:center;
  top:0; left:0;
`
