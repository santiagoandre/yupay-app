import React from 'react'
import styled from 'styled-components'
import FileInputPreview from './FileInputPreview'
import {  StyledButton , WrapperBox , Label} from 'admin-bro/components'
import Select from 'react-select'
import resourceService from '../services/resourceService.js'
import questionService from '../services/questionService.js'


export default class Question extends React.Component {

  constructor(props) {
    super(props)

    this.handleSubmit = this.handleSubmit.bind(this)
    this.selectHandler = this.selectHandler.bind(this)
    this.getResources = this.getResources.bind(this)
    const { record } = props
    this.state = {
      params: {images:[]},
      resources:[]
    }
    this.getResources()

  }
   handleSubmit(){

     questionService.new(this.state.params).then(res=>alert("Recurso creado")).catch(err=>alert("Error creando el recurso"))
   }
   selectHandler(key,value){

     if(typeof key == 'number'){
       const aux = value
       value = this.state.params.images
       value[key] = aux//el vector de imagenes con la nueva que llego en la variable value
       key = 'images'// se cambia todo el vector images por ^
     }
     this.setState({
       params:{...this.state.params, [key]:value}
     })
   }
   getResponseTimes(){
     const options = [
        { value: 30, label:  '30' },
        { value: 60,  label:  '60' },
        { value: 90,  label:  '90' },
        { value: 120, label: '120' }
    ]
    return options
   }
   getAnswers(){
     const options = [
       { value: 0, label:  'Respuesta 1(Fila 1)' },
       { value: 1, label:  'Respuesta 2(Fila 1)' },
       { value: 2, label:  'Respuesta 3(Fila 2)' },
       { value: 3, label:  'Respuesta 4(Fila 2)' }
     ]
    return options
   }
   getResources(){
     var resources =
       resourceService.getResources().then(res => {
           let resources = []
           res.data.map(resource => {
             resources.push({
               value: resource.id,
               label: resource.title
             })
           })
           this.setState({
             resources: resources
           })
         })
  }
  render() {
    const { resource } = this.props
    const properties = resource.editProperties
    const { record } = this.state
    const [ resourceProp, responseTimeProp,rightIndexProp] = properties
    //console.log(resourceProp)
    //console.log(responseTimeProp)
    const rows = [1,3]
    return (
      <WrapperBox border>
            <Label> {resourceProp.label}</Label>
            <Select options={this.state.resources} onChange={resource=>this.selectHandler('resource',resource.value)} />
            <br/>
            <Label> {responseTimeProp.label}</Label>
            <Select options={this.getResponseTimes()} onChange={time=> this.selectHandler('time',time.value)} />
            <br/>
            <FileInputPreview accept = "image/*" label="Seleccione la pregunta"  onChange = {(image) => this.selectHandler(0, image)}/>
            { rows.map(number =>
              <Row key={"row"+number.toString()}>
                <FileInputPreview accept = "image/*" key={"answer"+number.toString()} label= {"Seleccione la respuesta " + number.toString()}  onChange = {(image) => this.selectHandler(number, image)}/>
                <FileInputPreview accept = "image/*" key={"answer"+(number+1).toString()} label= {"Seleccione la respuesta " + (number+1).toString()}  onChange = {(image) => this.selectHandler(number+1, image)}/>
              </Row>

            )}
            <br/>
            <Label> {rightIndexProp.label}</Label>
            <Select options={this.getAnswers()} onChange={rightIndex=> this.selectHandler('rightIndex',rightIndex.value)} />
            <br/>
            <StyledButton  className="is-primary" onClick={this.handleSubmit}>
              <i className="icomoon-save" />
              <span className="btn-text">Guardar</span>
            </StyledButton>
      </WrapperBox>
    )
  }
}
const Checkbox = styled.input.attrs({
  type:"checkbox",
  className:"checkbox",
})``

const Row = styled.div
`
  width: 100%;
  height: 50%;
  display: flex;
  justify-content: space-around;
`
