import React from 'react'
import styled from 'styled-components'
import FileInputPreview from './FileInputPreview'
import { StyledInput, StyledButton , WrapperBox , Label} from 'admin-bro/components'
import Select from 'react-select'


import subtopicService from '../services/subtopicService.js'
import resourceService from '../services/resourceService.js'

export default class Resource extends React.Component {
  constructor(props) {
    super(props)

    this.handleSubmit = this.handleSubmit.bind(this)
    this.selectHandler = this.selectHandler.bind(this)
    this.getSubtopics = this.getSubtopics.bind(this)
    this.processResources = this.processResources.bind(this)
    const { record } = props
    this.state = {
      params: {},
      subtopics:[],
      errors:{},
      properties: {}
    }
    this.getSubtopics()
    this.properties  = this.processResources()

  }
  processResources(){
    const resources_vector = this.props.resource.editProperties
    var resources_dic = {}
    for(var pos in resources_vector ){
      const resource = resources_vector[pos]
      resources_dic[resource.name] = resource
    }
    resources_dic['thumbnail'] = {name: 'thumbnail',label: 'Imagen minuatura'}
    resources_dic['file'] = {name: 'file',label: 'Archivo pdf o video'}
    return resources_dic
  }
   handleSubmit(){
     //console.log(this.state.params);
     const params = this.props.resource.editProperties
     var error = {}
     for(var pos in params){
       const param = params[pos]
       if(!this.state.params[param.name]){
        error[param.name] = ` ${param.label }  es obligatorio`
      }
     }
     this.setState({
       errors:error
     })
     //console.log(error.data);
     if(Object.keys(error).length != 0){
    }else{
      console.log("send");
     resourceService.new(this.state.params).then(res=>alert("Recurso creado")).catch(err=>alert("Error creando el recurso"))
    }
   }
   selectHandler(key,value){
     //console.log(value)

       this.setState({
         params:{...this.state.params, [key]:value}
       })

   }
   getSubtopics(){
     var subtopics =
       subtopicService.getSubtopics().then(res => {
           let subtopics = []
           res.data.map(resource => {
             subtopics.push({
               value: resource.id,
               label: resource.title
             })
           })
           this.setState({
             subtopics: subtopics
           })
         })
   }
  render() {
    //console.log(resource)
    const p = this.properties

    const error = this.state.errors
    return (
      <WrapperBox border>
        <Label> {p['title'].label}</Label>
        <StyledInput
          type="text"
          className="input"
          onChange={event=>this.selectHandler(p['title'].name,event.target.value)}
        />
      {error[p['title'].name] && (
        <div className="help is-danger">{error[p['title'].name]}</div>
      )}
        <br/><br/>
        <Label> {p['author'].label}</Label>
        <StyledInput
          type="text"
          className="input"
          onChange={event=>this.selectHandler(p['author'].name,event.target.value)}
        />
      {error[p['author'].name] && (
        <div className="help is-danger">{error[p['author'].name]}</div>
      )}
        <br/><br/>
        <Label> {p['learningStyle'].label}</Label>
        <Select options={p['learningStyle'].availableValues} onChange={value=>this.selectHandler(p['learningStyle'].name,value.value)} />
      {error[p['learningStyle'].name] && (
        <div className="help is-danger">{error[p['learningStyle'].name]}</div>
      )}
        <br/>
        <Label> {p['intelligence'].label}</Label>
        <Select options={p['intelligence'].availableValues} onChange={value=>this.selectHandler(p['intelligence'].name,value.value)} />
      {error[p['intelligence'].name] && (
        <div className="help is-danger">{error[p['intelligence'].name]}</div>
      )}
        <br/>
        <Label> {p['id_subtopic'].label}</Label>
        <Select options={this.state.subtopics} onChange={subtopic=>this.selectHandler(p['id_subtopic'].name,subtopic.value)} />
      {error[p['id_subtopic'].name] && (
        <div className="help is-danger">{error[p['id_subtopic'].name]}</div>
      )}
        <br/>
        <Label> {p['thumbnail'].label}</Label>
        <FileInputPreview accept = "image/*" label="Seleccione la imagen minuatura"  onChange = {(image) => this.selectHandler(p['thumbnail'].name, image)}/>
      {error[p['thumbnail'].name] && (
        <div className="help is-danger">{error[p['thumbnail'].name]}</div>
      )}
        <br/>
        <Label> {p['file'].label}</Label>
        <FileInputPreview accept = "video/*,application/pdf" label="Seleccione el pdf o video"  onChange = {(image) => this.selectHandler(p['file'].name, image)}/>
      {error[p['file'].name] && (
        <div className="help is-danger">{error[p['file'].name]}</div>
      )}
        <br/>
        <StyledButton  className="is-primary" onClick={this.handleSubmit}>
          <i className="icomoon-save" />
          <span className="btn-text">Guardar</span>
        </StyledButton>
      </WrapperBox>
  )
}

}
