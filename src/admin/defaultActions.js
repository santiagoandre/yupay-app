module.exports = {
  show: {
    isAccessible: false,
  },
  new: {
    label: 'Crear',
    isAccessible: true,
    handler: async (request, response, data) => {
    if (request.method === 'post') {
      let record = await data.resource.build(request.payload.record)
      record = await record.save()
      if (record.isValid()) {
        return {
          redirectUrl: data.h.resourceActionUrl({ resourceId: data.resource.id(), actionName: 'list' }),
          record: record.toJSON(data.currentAdmin),
        }
      }

      console.log("Erro to save")
      console.log(record.toJSON(data.currentAdmin))//aqui estan lo mensajes de error que se muestran en la interfaz
      return { record: record.toJSON(data.currentAdmin) }
    }
    // TODO: add wrong implementation error
    throw new Error('new action can be invoked only via `post` http method')
  }
  },
  edit: {
    label: 'Editar',
    isAccessible: true,
    handler: async (request, response, data) => {
      const { record } = data
      if (request.method === 'get') {
        return { record: record.toJSON(data.currentAdmin) }
      }
      await record.update(request.payload.record)
      if (record.isValid()) {
        return {
            redirectUrl: data.h.resourceActionUrl({ resourceId: data.resource.id(), actionName: 'list' }),
            record: record.toJSON(data.currentAdmin),
        }
      }
      return { record: record.toJSON(data.currentAdmin) }
    }
  },
  list: {
    label: "Todos los registros",
    showFilter: false,
  },
  delete: {
    isAccessible:true,
    label: 'Eliminar',
    guard: '¿Desea eliminar este registro?'
  }
}
