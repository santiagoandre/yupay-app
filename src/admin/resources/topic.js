
module.exports = {
  name: 'Temas',
  properties: {
    title: {
      label: 'Titulo',
      isTitle: true,
    },
    id_course: {
      label: 'Curso'
    }
  }

}
