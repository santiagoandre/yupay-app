const AdminBro = require('admin-bro')

module.exports = {
  name: 'Subtemas',
  properties: {
    title: {
      label: 'Título',
      isTitle: true,
    },
    id_topic: {
      label: 'Tema'
    }
  }

}
