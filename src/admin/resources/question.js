const AdminBro = require('admin-bro')
module.exports = {
  name: 'Preguntas',
  properties: {
    id_resource: {
      label : 'Recurso asociado'
    },
    responseTime: {
      label: 'Tiempo para responder(segundos)',
      type: 'number',
      availableValues: [
        {value: '30' , label: '30 segundos'},
        {value: '60' , label: '60 segundos'},
        {value: '90' , label: '90 segundos'}
      ]
    },
    rightIndex: {
      isVisible: true,
      label: 'Respuesta correcta'
    }
  },
  actions:{
      show: {
        isAccessible: false,
      },
      new:{
        name: 'new',
        actionType: 'resource',
        label: 'Crear',
        isAccessible: true,
        component: AdminBro.bundle('../components/question')
      },
      edit:{
        label: "Editar",
        isAccessible:true
      },
      list: {
        label: "Todos los registros",
        showFilter: false,
      },
      delete: {
        isAccessible:false,
        label: 'Eliminar',
        guard: "¿Desea eliminar esta pregunta?"
      }
  }

}
