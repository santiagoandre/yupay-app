const resourceService = require('../services/resourceService')
const AdminBro = require('admin-bro')

module.exports = {
  name: 'Recurso',
  properties: {
    title: {
      label: 'Título',
      isTitle: true,
    },
    author: {
			label: 'Autor'
		},
		type: {
		    isVisible:false
		},
		learningStyle: {
      label: 'Estilo de aprendizaje'
		},
		intelligence: {
      label:'Tipo de inteligencia'
		},
		id_subtopic: {
      label:'Subtema'
		},
		views: {
      isVisible: { list: true, filter: true, show: true, edit: false, new: false},
      label: 'Visualizaciones'
		},
    thumbnail: {
      label: 'Imagen miniatura',
      isVisible: { list: false, filter: false, show: false, edit: false, new: true}
    },
    file: {
      label: 'Archivo pdf o video',
      isVisible: { list: false, filter: false, show: false, edit: false, new: true}
    }
  },
  actions:{
      show: {
        isAccessible: false,
      },
      new:{
        name: 'new',
        actionType: 'resource',
        label: 'Crear',
        isAccessible: true,
        component: AdminBro.bundle('../components/resource')
      },
      edit:{
        label: "Editar",
        isAccessible:true
      },
      list: {
        label: "Todos los registros",
        showFilter: false,
      },
      delete: {
        isAccessible:true,
        label: 'Eliminar',
        guard: "¿Desea eliminar este recurso?"
      }
  }
}
