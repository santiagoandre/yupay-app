  const AdminBro = require('admin-bro')

  module.exports = {
    name: 'Usuarios',
    properties: {
      fullName: {
        label: 'Nombre',
        isTitle: true
      },
      username: {
        label: 'Nombre de usuario'
      },
      password: {
        isVisible: false
      },
      age: {
        label: 'Edad'
      },
      gender: {
        label: 'Género'
      },
      grade: {
        label: 'Grado'
      },
      avatar: {
        isVisible: false
      },
      learningStyle: {
        label: 'Estilo de aprendizaje',
        isVisible: false
      },
      motivational_level: {
        label: 'Nivel motivacional',
        isVisible:false
      },
      intelligence: {
        label: 'Tipo de de inteligencia'
      },
      coins: {
        label: 'Monedas'
      }

    },
    actions: {
      show: {
        isAccessible: false,
      },
      new: {
        isAccessible: false,
      },
      edit: {
        isAccessible: false,
      },
      list: {
        label: "Todos los usuarios.",
        showFilter: false,
      },
      delete: {
        isAccessible: true
      }
    }

  }
