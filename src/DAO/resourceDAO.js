module.exports = app => {
  const Resource = app.db.models.resource;
  const resource ={
    all : () => (Resource.findAll({})),
    insert : (resource) =>(Resource.create(resource)),
    find : (id) =>(Resource.findByPk(id)),
    count: ()=>Resource.count(),
    delete: (id)=>Resource.destroy({where:{id:id}})
  }
return resource;

}
