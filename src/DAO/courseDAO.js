module.exports = app => {
  const Course = app.db.models.course;
  const course ={
    all : async() => (Course.findAll({})),
    insert : async(course) =>(Course.create(course)),
    find: async(course) =>(Course.findOne(course))
  }
  return course;
}
