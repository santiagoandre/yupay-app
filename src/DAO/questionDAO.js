module.exports = app => {
  const Question = app.db.models.question;
  const question = {
    all: () => (Question.findAll({})),
    insert: (question) => (Question.create(question)),
    select: (id_resource) => (Question.findAll({
      where: {
        id_resource: id_resource
      }
    })),
    count: ()=>Question.count(),
    delete: (id)=>Question.destroy({where:{id:id}})
  }
  return question;

}
