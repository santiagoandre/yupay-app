module.exports = app => {
  const Subtopic = app.db.models.subtopic;
  const subtopic ={
    all : async() => (Subtopic.findAll({})),
    insert : async(subtopic) =>(Subtopic.create(subtopic))
  }
  return subtopic;

}
