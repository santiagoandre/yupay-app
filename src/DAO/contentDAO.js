const Sequelize = require('sequelize');
module.exports = app => {
  const Topic = app.db.models.topic;
  const Subtopic = app.db.models.subtopic;
  const Resource = app.db.models.resource;
  const content = {

    all: async () => {
      return Topic.findAll({
        include: [{
          model: Subtopic,
          as: 'subtopics',
          include: [{
            model: Resource, 
            as: 'resources'
          }]
        }]
      });
    }
  };
  return content;

}
