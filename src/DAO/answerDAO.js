module.exports = app => {
  const Answer = app.db.models.answer;
  const answer ={
    all : async() => (Answer.findAll({})),
    insert : async(answer) =>(Answer.create(answer))
  }
  return answer;

}
