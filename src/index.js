const express = require('express');
const consign = require('consign');

const app = express();

// Routes
consign({cwd: __dirname})
  .include('libs/files.js')
  .include('libs/sequelize.js')
  .then('db.js')
  .then('libs/passport.js')
  .then('admin/index.js')
  .then('libs/middlewares.js')
  .then('libs/multer.js')
  .then('libs/passport.js')
  .then('DAO')
  .then('controllers')
  .then('libs/routes')
  .then('libs/boot.js')
  .into(app);
