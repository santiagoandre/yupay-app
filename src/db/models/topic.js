/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('topic', {
		
		title: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		id_course: {
			type: DataTypes.INTEGER,
			allowNull: false,
			references: {
				model: 'course',
				key: 'id'
			}
		}
	}, {
		tableName: 'topic',
		timestamps: false
	});
};
