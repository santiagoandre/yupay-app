/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('resource', {
		
		title: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		author: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		type: {
			type: DataTypes.TEXT,
			values: ['pdf','video'],
			defaultValue: "pdf"
		},
		learningStyle: {
			type: DataTypes.TEXT,
			allowNull: false,
			values: ['Activo','Pragmático','Reflexivo','Teórico']
		},
		intelligence: {
			type: DataTypes.TEXT,
			allowNull: false,
			values: ['cinetico-corporal','espacial','logico-matematica','musical','naturalista','linguistica','interpersonal']
		},
		id_subtopic: {
			type: DataTypes.INTEGER,
			allowNull: false,
			references: {
				model: 'subtopic',
				key: 'id'
			}
		},
		views: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	}, {
		tableName: 'resource',
		timestamps: false
	});
};
