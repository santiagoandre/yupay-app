/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const subtopic = sequelize.define('subtopic', {
		
		title: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		id_topic: {
			type: DataTypes.INTEGER,
			allowNull: false,
			references: {
				model: 'topic',
				key: 'id'
			}
		}
	}, {
		tableName: 'subtopic',
		timestamps: false
	});

	return subtopic;
};
