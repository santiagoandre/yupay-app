/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('course', {

    name: {
      type: DataTypes.TEXT,  
      allowNull: false
    }
  }, {
    tableName: 'course',
    timestamps: false
  });
};
