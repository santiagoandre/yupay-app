/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('question', {
    
    id_resource: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'resource',
        key: 'id'
      }
    },
    responseTime: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    rightIndex: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    tableName: 'question',
    timestamps: false
  });
};
