/* jshint indent: 1 */
const bcrypt = require('bcrypt-nodejs');
module.exports =  function(sequelize, DataTypes) {
	const Model = sequelize.define('user', {
	
		fullName: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		username: {
			 type: DataTypes.TEXT,
		   allowNull: false,
			 unique: true
	  },
		password: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		age: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		gender: {
			type: DataTypes.TEXT,
			allowNull: false,
			values: ['Masculino','Femenino','Otro']
		},
		grade: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		avatar: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		learningStyle: {
			type: DataTypes.TEXT,
			allowNull: false,
			values: ['Activo','Pragmático','Reflexivo','Teórico']
		},
		motivational_level: {
			type: DataTypes.TEXT
		},
		intelligence: {
			type: DataTypes.TEXT,
			allowNull: false,
			values: ['cinetico-corporal','espacial','logico-matematica','musical','naturalista','linguistica','interpersonal']
		},
		coins: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	},
	{
		hooks: {
			beforeCreate : (user)  => {
				if(user.password){
					user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null);
				}
			},
			beforeUpdate : (user)  => {
				if(user.password){
					user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null);
				}
			}
		 },
		tableName: 'user',
		timestamps: false
	});
	Model.prototype.verifyPassword =  function(password,cb) {
		bcrypt.compare(password,this.password,(err,equals) =>{
			if(err){
				cb(err);
			}
			cb(null,equals);
		});

	};
	return Model;
};
